package com.ng.commonvalidationbpel;

import static org.assertj.core.api.Assertions.assertThat;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.ng.sit.configuration.SecurityConfiguration;
import com.ng.sit.configuration.SitConfiguration;
import com.ng.sit.errors.PredefinedErrors;
import com.ng.sit.errors.WASITException;
import com.ng.sit.services.security.model.TokenAndSignature;
import com.ng.sit.utils.Utils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CommonBPEL {

	@Autowired
	SitConfiguration config;

	@Autowired
	SecurityConfiguration secConf;

	@Autowired
	Utils utility;

	private String replyTo;

	@Before
	public void prepare() {
		if(config.getReplyto()!=null && config.getReplyto().trim().length()>0) {
			replyTo=config.getReplyto();
		}
		else {
			replyTo=config.getApihostname();
		}
	}


	@Test
	public void CorrectPayloadEDTScopeOK() throws WASITException, JSONException {
		//Prepare the Test
		final String payload=utility.getPayloadAsString("payloads/submission.json");
		final TokenAndSignature secResp= utility.signPayload(secConf.getAudience(), secConf.getScopeEDT(), payload);

		//Run the Test
		final ResponseEntity<String> resp= utility.postServiceCall(config.getApihostname(),
				"/Validation",
				payload,
				secResp.getSignature(),
				secResp.getToken(),
				replyTo,
				secConf.getAudience(),
				secConf.getScopeEDT()
				);

		//Verify the results
		assertThat(resp.getBody().toString()).contains("\"message\": \"Successful Request\"");
		assertThat(resp.getStatusCode()).isSameAs(HttpStatus.OK);

	}

	@Test
	public void CorrectPayloadEDLScopeOK() throws WASITException {

		//Prepare the Test
		final String payload=utility.getPayloadAsString("payloads/redeclaration.json");
		final TokenAndSignature secResp= utility.signPayload(secConf.getAudience(), secConf.getScopeEDL(), payload);


		//Run the Test
		final ResponseEntity<String> resp= utility.postServiceCall(config.getApihostname(),
				"/Validation",
				payload,
				secResp.getSignature(),
				secResp.getToken(),
				replyTo,
				secConf.getAudience(),
				secConf.getScopeEDL()
				);


		//Verify the results
		assertThat(resp.getBody().toString()).contains("\"message\": \"Successful Request\"");
		assertThat(resp.getStatusCode()).isSameAs(HttpStatus.OK);

	}

	@Test
	public void WrongMarketParticipant() throws WASITException {

		//Prepare the Test
		final String payload=utility.getPayloadAsString("payloads/submissionNOK.json");
		final TokenAndSignature secResp= utility.signPayload(secConf.getAudience(), secConf.getScopeEDT(), payload);

		//Run the Test
		final ResponseEntity<String> resp= utility.postServiceCall(config.getApihostname(),
				"/Validation",
				payload,
				secResp.getSignature(),
				secResp.getToken(),
				replyTo,
				secConf.getAudience(),
				secConf.getScopeEDT()
				);


		//Verify the results
		assertThat(resp.getBody().toString()).contains("\"message\": \""+PredefinedErrors.WABE1001.getMessage()+"\"");
		assertThat(resp.getBody().toString()).contains("\"detail\": \""+PredefinedErrors.WABE1001.getDetail()+"\"");
		assertThat(resp.getBody().toString()).contains("\"code\": \""+PredefinedErrors.WABE1001.getCode());
		assertThat(resp.getStatusCode()).isSameAs(PredefinedErrors.WABE1001.getStatusCode());

	}

	@Test
	public void CurruptedPayload()  {
		//Prepare the Test
		String payload=utility.getPayloadAsString("payloads/redeclaration.json");

		final TokenAndSignature secResp= utility.signPayload(secConf.getAudience(), secConf.getScopeEDL(), payload);

		payload=utility.getPayloadAsString("payloads/redeclarationCorrupted.vm");

		//Run the Test
		final ResponseEntity<String> resp= utility.postServiceCall(config.getApihostname(),
				"/Validation",
				payload,
				secResp.getSignature(),
				secResp.getToken(),
				replyTo,
				secConf.getAudience(),
				secConf.getScopeEDL()
				);

		//Verify the results
		assertThat(resp.getStatusCode()).isSameAs(HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@Test
	public void EmptyPayload() throws WASITException, JSONException {
		//Prepare the Test
		final String payload="{}";

		final TokenAndSignature secResp= utility.signPayload(secConf.getAudience(), secConf.getScopeEDL(), payload);

		//Run the Test
		final ResponseEntity<String> resp= utility.postServiceCall(config.getApihostname(),
				"/Validation",
				payload,
				secResp.getSignature(),
				secResp.getToken(),
				replyTo,
				secConf.getAudience(),
				secConf.getScopeEDL()
				);

		//Verify the results
		assertThat(resp.getBody().toString()).contains("\"message\": \""+PredefinedErrors.WABE1001.getMessage()+"\"");
		assertThat(resp.getBody().toString()).contains("\"detail\": \""+PredefinedErrors.WABE1001.getDetail()+"\"");
		assertThat(resp.getBody().toString()).contains("\"code\": \""+PredefinedErrors.WABE1001.getCode());
		assertThat(resp.getStatusCode()).isSameAs(PredefinedErrors.WABE1001.getStatusCode());

	}

	@Test
	public void WrongScope() throws WASITException {

		//Prepare the Test
		final String payload=utility.getPayloadAsString("payloads/redeclaration.json");
		final TokenAndSignature secResp= utility.signPayload(secConf.getAudience(), secConf.getScopeEDL(), payload);


		//Run the Test
		final ResponseEntity<String> resp= utility.postServiceCall(config.getApihostname(),
				"/Validation",
				payload,
				secResp.getSignature(),
				secResp.getToken(),
				replyTo,
				secConf.getAudience(),
				secConf.getScopeEDT()
				);


		//Verify the results
		//Verify the results
		assertThat(resp.getBody().toString()).contains("\"message\": \""+PredefinedErrors.WABE0001.getMessage()+"\"");
		assertThat(resp.getBody().toString()).contains("\"detail\": \"The Scope is incorrect\"");
		assertThat(resp.getBody().toString()).contains("\"code\": \""+PredefinedErrors.WABE0001.getCode());
		assertThat(resp.getStatusCode()).isSameAs(PredefinedErrors.WABE0001.getStatusCode());

	}

	@Test
	public void WrongToken() throws WASITException {

		//Prepare the Test
		final String payload=utility.getPayloadAsString("payloads/submission.json");
		final TokenAndSignature secResp= utility.signPayload(secConf.getAudience(), secConf.getScopeEDT(), payload);

		//Run the Test
		final ResponseEntity<String> resp= utility.postServiceCall(config.getApihostname(),
				"/Validation",
				payload,
				secResp.getSignature(),
				"garbage"+secResp.getToken(),
				replyTo,
				secConf.getAudience(),
				secConf.getScopeEDT()
				);


		//Verify the results
		assertThat(resp.getBody().toString()).contains("\"message\": \""+PredefinedErrors.WABE0001.getMessage()+"\"");
		assertThat(resp.getBody().toString()).contains("\"detail\": \"Token is invalid\"");
		assertThat(resp.getBody().toString()).contains("\"code\": \""+PredefinedErrors.WABE0001.getCode());
		assertThat(resp.getStatusCode()).isSameAs(PredefinedErrors.WABE0001.getStatusCode());

	}

	@Test
	public void WrongSignature() throws WASITException {

		//Prepare the Test
		final String payload=utility.getPayloadAsString("payloads/submission.json");
		final TokenAndSignature secResp= utility.signPayload(secConf.getAudience(), secConf.getScopeEDT(), payload);

		//Run the Test
		final ResponseEntity<String> resp= utility.postServiceCall(config.getApihostname(),
				"/Validation",
				payload,
				"garbage"+secResp.getSignature(),
				secResp.getToken(),
				replyTo,
				secConf.getAudience(),
				secConf.getScopeEDT()
				);

		//Verify the results
		assertThat(resp.getBody().toString()).contains("\"message\": \""+PredefinedErrors.WABE0002.getMessage()+"\"");
		assertThat(resp.getBody().toString()).contains("\"code\": \""+PredefinedErrors.WABE0002.getCode());
		assertThat(resp.getStatusCode()).isSameAs(PredefinedErrors.WABE0002.getStatusCode());

	}

	@Test
	public void WrongSignatureAndToken() throws WASITException {

		//Prepare the Test
		final String payload=utility.getPayloadAsString("payloads/submission.json");
		final TokenAndSignature secResp= utility.signPayload(secConf.getAudience(), secConf.getScopeEDT(), payload);


		//Run the Test
		final ResponseEntity<String> resp= utility.postServiceCall(config.getApihostname(),
				"/Validation",
				payload,
				"garbage"+secResp.getSignature(),
				"garbage"+secResp.getToken(),
				replyTo,
				secConf.getAudience(),
				secConf.getScopeEDT()
				);

		//Verify the results
		assertThat(resp.getBody().toString()).contains("\"message\": \""+PredefinedErrors.WABE0001.getMessage()+"\"");
		assertThat(resp.getBody().toString()).contains("\"detail\": \"Token is invalid\"");
		assertThat(resp.getBody().toString()).contains("\"code\": \""+PredefinedErrors.WABE0001.getCode());
		assertThat(resp.getStatusCode()).isSameAs(PredefinedErrors.WABE0001.getStatusCode());

	}

	@Test
	public void MissingSignatureAndToken() throws WASITException {

		//Prepare the Test
		final String payload=utility.getPayloadAsString("payloads/submission.json");
		//Run the Test
		final ResponseEntity<String> resp= utility.postServiceCall(config.getApihostname(),
				"/Validation",
				payload,
				"",
				"",
				replyTo,
				secConf.getAudience(),
				secConf.getScopeEDT()
				);


		//Verify the results
		assertThat(resp.getBody().toString()).contains("\"code\": \""+PredefinedErrors.WATE0008.getCode());
		assertThat(resp.getStatusCode()).isSameAs(PredefinedErrors.WATE0008.getStatusCode());
		assertThat(resp.getBody().toString()).contains("\"message\": \""+PredefinedErrors.WATE0008.getMessage()+"\"");

	}
}

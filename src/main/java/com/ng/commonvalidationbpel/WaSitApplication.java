package com.ng.commonvalidationbpel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class WaSitApplication  {

	public static void main(String[] args) {
		SpringApplication.run(WaSitApplication.class, args);
	}

}

